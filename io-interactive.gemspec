# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'io/interactive/version'

Gem::Specification.new do |spec|
  spec.name          = "io-interactive"
  spec.version       = IO::Interactive::VERSION
  spec.authors       = ["nomeaning"]
  spec.email         = ["nomeaning@mma.club.uec.ac.jp"]
  spec.summary       = %q{interact with IO}
  spec.description   = %q{You can interact with IO}
  spec.homepage      = "https://bitbucket.org/nomeaning777/io-interactive"
  spec.license       = "MIT"

  spec.files         = `git ls-files -z`.split("\x0")
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]

  spec.add_development_dependency "bundler", "~> 1.7"
  spec.add_development_dependency "rake", "~> 10.0"
end
