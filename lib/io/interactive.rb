require 'set'
require "io/interactive/version"

class IO
  module Interactive
    def interactive!(input = STDIN, output = STDOUT)
      close = Set.new
      while close.size < 2
        inputs = [input, self] - close.to_a
        rs = IO.select(inputs, [], [], 10)
        next unless rs
        rs = rs[0]
        begin
          if rs[0] == input
            data = input.read_nonblock(1000)
            write data if data != ''
          elsif rs[0] == self
            data = read_nonblock(1000)
            output.write data if data != ''
          end
        rescue EOFError => e
          close << rs[0]
          rs[0].close_read
          break if rs[0] == self
        end
      end
    end
  end

  include Interactive
end
